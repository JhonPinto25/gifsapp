import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchResponse } from '../interfaces/gifs.interfaces';

//const GIPHY_API_KEY = 'cRLsrnawH1HyBgWCsNT0PBxMG10BYCbj';

@Injectable({
  providedIn: 'root',
})
export class GifsService {
  private _tagsHistory: string[] = [];
  private apiKeyGiphy: string = 'cRLsrnawH1HyBgWCsNT0PBxMG10BYCbj';
  private serviceUrl: string = 'https://api.giphy.com/v1/gifs';

  public gifList: Gif[] = [];

  constructor(private http: HttpClient) {
    this.loadLocalStorage();
    console.log('Gifs service is ready');
  }

  get tagsHistory() {
    return [...this._tagsHistory];
  }

  private organizeHistory(tag: string) {
    tag = tag.toLocaleLowerCase();

    //Elimina el tag repetido y lo coloca al principio
    /*  for (let index = 0; index < this._tagsHistory.length; index++) {
      if (tag === this._tagsHistory[index]) {
        this._tagsHistory.splice(index, 1);

        console.log(this._tagsHistory);
      }
    } */
    //FH
    if (this._tagsHistory.includes(tag)) {
      this._tagsHistory = this._tagsHistory.filter((oldTag) => oldTag !== tag);
    }
    this._tagsHistory.unshift(tag);
    this._tagsHistory = this._tagsHistory.splice(0, 10);
    this.saveLocalStorage(); //guarda en el local storage
  }

  private saveLocalStorage(): void {
    localStorage.setItem('history', JSON.stringify(this._tagsHistory));
  }

  private loadLocalStorage(): void {
    if (!localStorage.getItem('history')) {
      return;
    }
    this._tagsHistory = JSON.parse(localStorage.getItem('history')!);

    if (this._tagsHistory.length === 0) {
      return;
    }

    this.searchTag(this._tagsHistory[0]);
  }

  searchTag(tag: string): void {
    if (tag.length === 0) {
      return;
    }
    this.organizeHistory(tag);

    // fetch retorna una promise
    /*  fetch('https://api.giphy.com/v1/gifs/search?api_key=cRLsrnawH1HyBgWCsNT0PBxMG10BYCbj&q=valorant&limit=10')
    .then(resp => resp.json())
    .then(data => console.log(data)); */
    // otra forma de hacerlo que es igual a la anterior es la siguiente, sin embargo, angular tiene su propio objeto para peticiones http muy fuerte,
    // alternativa axios
    /* const resp = await fetch('https://api.giphy.com/v1/gifs/search?api_key=cRLsrnawH1HyBgWCsNT0PBxMG10BYCbj&q=valorant&limit=10');
    const data = await resp.json();
    console.log(data); */
    const params = new HttpParams()
      .set('api_key', this.apiKeyGiphy)
      .set('limit', '10')
      .set('q', tag);

    this.http
      .get<SearchResponse>(`${this.serviceUrl}/search`, { params })
      .subscribe((resp) => {
        this.gifList = resp.data;
        //console.log(this.gifList);
      });
  }
}
