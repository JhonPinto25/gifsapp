import { Component } from '@angular/core';

import { GifsService } from '../../../gifs/services/gifs.service';

@Component({
  selector: 'shared-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css',
})
export class SidebarComponent {
  tagHistory: string[] = [];

  constructor(private gifsService: GifsService) {
    /* this.tagHistory = this.gifsService.tagsHistory;
    console.log(this.tagHistory) ; */
  }

  public get tags() {
    return this.gifsService.tagsHistory;
  }

  searchGifs(tag: string) {
    this.gifsService.searchTag(tag);
  }
}
